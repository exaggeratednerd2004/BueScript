# BueScript

BueScript is a scripting language designed to be a mix of JavaScript and Python together to make the ultimate language.
The official runtime is written in C++ using C++14 as the C++ standard. Designed to be fast, efficient and simple to write, test and debug code with simplicity.